<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('register');
    }

    public function handleForm(Request $request)
    {
        $namaPertama = $request['first-name'];
        $namaKedua = $request['last-name'];

        return view('welcome', compact('namaPertama', 'namaKedua'));
    }
    
    public function welcome()
    {
        return view('welcome');
    }
}
