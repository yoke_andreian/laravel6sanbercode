@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="card">
        <form action="/pertanyaan/{{$post->id}}" method="POST">
            <div class="card-header">
                Form Edit Pertanyaan {{$post->id}}
            </div>
            <div class="card-body">
            @csrf
            @method('put')
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" name="judul" id="judul" class="form-control" placeholder="Masukan Judul" value="{{old('judul', $post->judul)}}">
                </div>
                <div class="form-group">
                    <label for="isi">Isi Pertanyaan</label>
                    <textarea name="isi" id="isi" cols="30" rows="10" class="form-control">{{old('isi', $post->isi)}}</textarea>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>

@endsection