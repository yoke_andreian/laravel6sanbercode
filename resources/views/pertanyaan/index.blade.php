@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <h1 class="mb-3">Hallo Selamat Datang <a href="/pertanyaan/create" class="btn btn-primary btn-sm">Tambah</a></h1>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Judul</th>
                    <th class="text-center">Konten / isi</th>
                    <th class="text-center">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse($post as $key=>$value)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$value->judul}}</td>
                        <td>{{$value->isi}}</td>
                        <td>
                            <a href="/pertanyaan/{{$value->id}}" class="btn btn-primary btn-sm">Show</a>
                            <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <form action="/pertanyaan/{{$value->id}}" method="POST" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4">
                            <div class="text-center">No Data </div>
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection