@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="card">
            <div class="card-header">
                Detail dari ID {{$post->id}}
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" name="judul" id="judul" class="form-control" placeholder="Masukan Judul"
                        value="{{$post->judul}}" readonly>
                </div>
                <div class="form-group">
                    <label for="isi">Isi Pertanyaan</label>
                    <textarea name="isi" id="isi" cols="30" rows="10"
                        class="form-control" readonly>{{$post->isi}}</textarea>
                </div>
            </div>
            <div class="card-footer">
                <a href="/pertanyaan" class="btn btn-primary">Kembali</a>
            </div>
    </div>
</div>

@endsection