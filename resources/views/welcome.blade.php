<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
</head>

<body>
    <div class="container">
        <h1>
        <strong>SELAMAT DATANG {{$namaPertama}} {{$namaKedua}} !</strong>
        </h1>
        <h4>
            <strong>Terima kasih telah bergabung di SanberBook. Social Media kita bersama.</strong>
        </h4>
    </div>
</body>

</html>